import {Component, OnInit, HostListener, Inject} from '@angular/core';
import {DOCUMENT} from "@angular/platform-browser";
import {Image} from "../shared/image";

const CATEGORIES = [
    'animals',
    'sports',
    'nature'
];

const SERVICE_HEIGHT = 70 + 49;

const API_IMAGE_URL = 'http://lorempixel.com/';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    images: Image[];
    imagesNature;
    imagesSports;
    imagesAnimals;
    windowHeight;
    imageHeight;
    imageWidth;
    public rowsCount: number;

    constructor(@Inject(DOCUMENT) private document: Document) {
    }

    ngOnInit() {
        this.images = [];
        this.imagesNature = [];
        this.imagesSports = [];
        this.imagesAnimals = [];
        this.rowsCount = 3;
        this.updateWindowHeight();
        this.addImages(this.rowsCount);
    }

    getRandomFromArray(array) {
        return array[Math.floor(array.length * Math.random())];
    }

    updateWindowHeight() {
        this.windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        this.imageHeight = Math.round(((this.windowHeight - SERVICE_HEIGHT)/this.rowsCount) - 10);
        this.imageWidth = this.imageHeight;
    }

    addImages(count) {
        for ( let i = 0; i < count * 4; i++) {
            let newImage = new Image(this.imageWidth, this.imageHeight, this.getRandomFromArray(CATEGORIES), API_IMAGE_URL);
            this.images.push(newImage);
            this.filterImages(newImage);
        }
    }

    updateImagesHeight() {
        this.images.filter(image => {
            image.height = this.imageHeight;
            image.width = this.imageWidth;
        });
    }

    updateRowsCount(event) {
        if (event.target.value < 3 || event.target.value > 20) {
            return false;
        }
        this.addImages(event.target.value - this.rowsCount);
        this.rowsCount = event.target.value;
        this.updateWindowHeight();
        this.updateImagesHeight();
    }

    @HostListener("window:scroll", [])
    onWindowScroll() {
        if ((this.document.body.scrollTop + this.windowHeight) == this.document.body.offsetHeight) {
            this.addImages(this.rowsCount);
        }
    }

    @HostListener('window:resize', [])
    onResize() {
        this.updateWindowHeight();
        this.updateImagesHeight();
    }

    filterImages(image) {
        if (image.category.indexOf('nature') != -1) {
            this.imagesNature.push(image);
        } else if (image.category.indexOf('sports') != -1) {
            this.imagesSports.push(image);
        } else if (image.category.indexOf('animals') != -1) {
            this.imagesAnimals.push(image);
        }
    }
}
