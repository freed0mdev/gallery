export class Image {
    constructor(
        public width: number,
        public height: number,
        public category: string = '',
        public url: string = 'http://lorempixel.com/',
        public title: string = 'Default Title'
    ) {}
}